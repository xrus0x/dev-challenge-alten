package com.alten.technicalchallenge.application.facade;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.alten.technicalchallenge.application.mapper.ResponseMapper;
import com.alten.technicalchallenge.application.response.TweetResponse;
import com.alten.technicalchallenge.domain.service.TweetService;

/**
 * The Class TweetFacade.
 */
@Component
public class TweetFacade {

	/** The tweet service. */
	private final TweetService tweetService;
	
	/** The mapper. */
	private final ResponseMapper mapper;

	/**
	 * Instantiates a new tweet facade.
	 *
	 * @param tweetService the tweet service
	 * @param mapper the mapper
	 */
	@Autowired
	public TweetFacade(TweetService tweetService, ResponseMapper mapper) {
		this.tweetService = tweetService;
		this.mapper = mapper;
	}

	/**
	 * Gets the all tweets.
	 *
	 * @return the all tweets
	 */
	public List<TweetResponse> getAllTweets() {
		return mapper.listTweerToListTweetResponse(tweetService.getTweetsList());
	}

	/**
	 * Gets the all validated tweet by user.
	 *
	 * @param userId the user id
	 * @return the all validated tweet by user
	 */
	public List<TweetResponse> getAllValidatedTweetByUser(String userId) {
		return mapper.listTweerToListTweetResponse(tweetService.getValidatedTweetByUser(userId));
	}
	
	/**
	 * Validate tweet.
	 *
	 * @param tweetId the tweet id
	 */
	public void validateTweet(long tweetId) {
		tweetService.validateTweet(tweetId);
	}
}
