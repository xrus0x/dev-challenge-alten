package com.alten.technicalchallenge.application.facade;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import com.alten.technicalchallenge.application.mapper.ResponseMapper;
import com.alten.technicalchallenge.application.response.HashTagResponse;
import com.alten.technicalchallenge.domain.model.HashTag;
import com.alten.technicalchallenge.domain.service.HashTagService;

/**
 * The Class HashTagFacade.
 */
@Component
public class HashTagFacade {

	/** The hash tag service. */
	private final HashTagService hashTagService;
	
	/** The mapper. */
	private final ResponseMapper mapper;

	/**
	 * Instantiates a new hash tag facade.
	 *
	 * @param hashTagService the hash tag service
	 * @param mapper the mapper
	 */
	public HashTagFacade(HashTagService hashTagService, ResponseMapper mapper) {
		this.hashTagService = hashTagService;
		this.mapper = mapper;
	}

	/**
	 * Gets the top ten hash tag.
	 *
	 * @return the top ten hash tag
	 */
	public List<HashTagResponse> getTopTenHashTag() {
		List<HashTag> topTenHashTag = hashTagService.getHashTagList().stream()
				.sorted(Comparator.comparing(HashTag::getCount).reversed()).collect(Collectors.toList());
		
		int limit = (topTenHashTag.size()<10)?topTenHashTag.size():10;
		
 		return mapper.ListHashTagToListHashTagResponse(topTenHashTag.subList(0, limit));
	}

}
