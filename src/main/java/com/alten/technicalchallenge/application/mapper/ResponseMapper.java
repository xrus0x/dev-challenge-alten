package com.alten.technicalchallenge.application.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import com.alten.technicalchallenge.application.response.HashTagResponse;
import com.alten.technicalchallenge.application.response.TweetResponse;
import com.alten.technicalchallenge.domain.model.HashTag;
import com.alten.technicalchallenge.domain.model.Tweet;

/**
 * The Interface ResponseMapper.
 */
@Mapper(componentModel = "spring")
public interface ResponseMapper {
	
	/**
	 * Tweet to tweet response.
	 *
	 * @param source the source
	 * @return the tweet response
	 */
	@Mappings({
		@Mapping(source="user", target="userName"),
		@Mapping(source="text", target="message")
	})
	TweetResponse tweetToTweetResponse(Tweet source);
	
	/**
	 * List tweer to list tweet response.
	 *
	 * @param source the source
	 * @return the list
	 */
	List<TweetResponse> listTweerToListTweetResponse(List<Tweet> source);
	
	/**
	 * Hash tag to hash tag response.
	 *
	 * @param source the source
	 * @return the hash tag response
	 */
	HashTagResponse HashTagToHashTagResponse(HashTag source);
	
	/**
	 * List hash tag to list hash tag response.
	 *
	 * @param source the source
	 * @return the list
	 */
	List<HashTagResponse> ListHashTagToListHashTagResponse(List<HashTag> source);
}
