package com.alten.technicalchallenge.application.response;

import lombok.Data;

/**
 * Instantiates a new tweet response.
 */
@Data
public class TweetResponse {

	/** The id. */
	private long id;
	
	/** The user name. */
	private String userName;
	
	/** The message. */
	private String message;
}
