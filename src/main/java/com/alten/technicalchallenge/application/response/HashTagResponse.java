package com.alten.technicalchallenge.application.response;

import lombok.Data;

/**
 * Instantiates a new hash tag response.
 */
@Data
public class HashTagResponse {
	
	/** The hashtag. */
	private String hashtag;
}
