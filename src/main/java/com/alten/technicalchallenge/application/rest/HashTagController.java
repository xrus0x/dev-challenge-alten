package com.alten.technicalchallenge.application.rest;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alten.technicalchallenge.application.facade.HashTagFacade;
import com.alten.technicalchallenge.application.response.HashTagResponse;

/**
 * The Class HashTagController.
 */
@RestController
@RequestMapping("/hashtag")
public class HashTagController {

	/** The hash tag facade. */
	private final HashTagFacade hashTagFacade;

	/**
	 * Instantiates a new hash tag controller.
	 *
	 * @param hashTagFacade the hash tag facade
	 */
	public HashTagController(HashTagFacade hashTagFacade) {
		super();
		this.hashTagFacade = hashTagFacade;
	}

	/**
	 * Gets the top ten hash tag.
	 *
	 * @return the top ten hash tag
	 */
	@GetMapping(path = "/topTen", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<HashTagResponse>> getTopTenHashTag() {
		return new ResponseEntity<List<HashTagResponse>>(hashTagFacade.getTopTenHashTag(), HttpStatus.OK);
	}
}
