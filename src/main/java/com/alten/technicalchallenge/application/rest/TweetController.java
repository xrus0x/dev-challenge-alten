package com.alten.technicalchallenge.application.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alten.technicalchallenge.application.facade.TweetFacade;
import com.alten.technicalchallenge.application.response.TweetResponse;

/**
 * The Class TweetController.
 */
@RestController
@RequestMapping("/tweets")
public class TweetController {

	/** The Constant OK. */
	private static final String OK = "OK";

	/** The tweet facade. */
	private final TweetFacade tweetFacade;

	/**
	 * Instantiates a new tweet controller.
	 *
	 * @param tweetFacade the tweet facade
	 */
	@Autowired
	public TweetController(TweetFacade tweetFacade) {
		this.tweetFacade = tweetFacade;
	}

	/**
	 * Gets the tweets.
	 *
	 * @return the tweets
	 */
	@GetMapping(path = "/all", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<TweetResponse>> getTweets() {
		return new ResponseEntity<List<TweetResponse>>(tweetFacade.getAllTweets(), HttpStatus.OK);
	}

	/**
	 * Gets the validated tweet by user.
	 *
	 * @param userId the user id
	 * @return the validated tweet by user
	 */
	@GetMapping(path = "/{userId}/validated", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<TweetResponse>> getValidatedTweetByUser(
			@PathVariable(name = "userId", required = true) String userId) {
		return new ResponseEntity<List<TweetResponse>>(tweetFacade.getAllValidatedTweetByUser(userId), HttpStatus.OK);
	}

	/**
	 * Validated tweet.
	 *
	 * @param tweetId the tweet id
	 * @return the response entity
	 */
	@PutMapping(path = "/{tweetId}/validate")
	public ResponseEntity<String> validatedTweet(@PathVariable(name = "tweetId", required = true) long tweetId) {

		tweetFacade.validateTweet(tweetId);
		return new ResponseEntity<String>(OK, HttpStatus.OK);
	}
}
