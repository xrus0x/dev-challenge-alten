package com.alten.technicalchallenge.infrastructure.db.springdata.jpa.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import com.alten.technicalchallenge.domain.model.Tweet;
import com.alten.technicalchallenge.domain.repository.TweetRepository;
import com.alten.technicalchallenge.infrastructure.db.springdata.jpa.dbo.TweetDBO;
import com.alten.technicalchallenge.infrastructure.db.springdata.jpa.mapper.TweetMapper;

import lombok.extern.slf4j.Slf4j;

/**
 * The Class TweetDBORepository.
 */
@Repository
@Slf4j
public class TweetDBORepository implements TweetRepository {

	/** The tweet JPA repository. */
	private final TweetJPARepository tweetJPARepository;
	
	/** The mapper. */
	@Autowired
	private TweetMapper mapper;

	/**
	 * Instantiates a new tweet DBO repository.
	 *
	 * @param tweetJPARepository the tweet JPA repository
	 */
	@Autowired
	public TweetDBORepository(TweetJPARepository tweetJPARepository) {
		this.tweetJPARepository = tweetJPARepository;
		
	}

	/**
	 * Gets the all.
	 *
	 * @return the all
	 */
	@Override
	public List<Tweet> getAll() {
		return mapper.listTweetDBOToListTweet(tweetJPARepository.findAll());
	}

	/**
	 * Gets the validated twee by user.
	 *
	 * @param user the user
	 * @return the validated twee by user
	 */
	@Override
	public List<Tweet> getValidatedTweeByUser(String user) {
		return mapper.listTweetDBOToListTweet(tweetJPARepository.findByUserAndValid(user, true));
	}

	/**
	 * Validate tweet.
	 *
	 * @param idTweet the id tweet
	 */
	@Override
	public void validateTweet(long idTweet) {
		Optional<TweetDBO> tweet = tweetJPARepository.findById(Long.valueOf(idTweet));
		if(tweet.isPresent()) {
			TweetDBO validatedTweet = tweet.get();
			validatedTweet.setValid(true);
			tweetJPARepository.save(validatedTweet);
		}

	}

	/**
	 * Save.
	 *
	 * @param tweet the tweet
	 */
	@Override
	public void save(Tweet tweet) {
		try {
			tweetJPARepository.save(mapper.tweetToTweetDBO(tweet));
		} catch (DataAccessException ex) {
			log.error(ex.getMessage());
		}
		
	}

}
