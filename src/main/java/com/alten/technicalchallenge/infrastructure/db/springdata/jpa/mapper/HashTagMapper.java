package com.alten.technicalchallenge.infrastructure.db.springdata.jpa.mapper;

import java.util.List;

import org.mapstruct.Mapper;

import com.alten.technicalchallenge.domain.model.HashTag;
import com.alten.technicalchallenge.infrastructure.db.springdata.jpa.dbo.HashTagDBO;

/**
 * The Interface HashTagMapper.
 */
@Mapper(componentModel = "spring")
public interface HashTagMapper {
	
	/**
	 * Hash tag DBO to hash tag.
	 *
	 * @param source the source
	 * @return the hash tag
	 */
	HashTag hashTagDBOToHashTag(HashTagDBO source);
	
	/**
	 * Hash tag to hashtag DBO.
	 *
	 * @param source the source
	 * @return the hash tag DBO
	 */
	HashTagDBO hashTagToHashtagDBO(HashTag source);
	
	/**
	 * List hash tag DBO to list hash tag.
	 *
	 * @param source the source
	 * @return the list
	 */
	List<HashTag> listHashTagDBOToListHashTag(List<HashTagDBO> source);
	
	/**
	 * List hash tag to list hashtag DBO.
	 *
	 * @param source the source
	 * @return the list
	 */
	List<HashTagDBO> ListHashTagToListHashtagDBO(List<HashTag> source);
}
