package com.alten.technicalchallenge.infrastructure.db.springdata.jpa.conf;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Configuration
@Slf4j
@Data
@ConfigurationProperties("spring.datasource")
@EntityScan(basePackages = "com.alten.technicalchallenge.infrastructure.db.springdata.jpa.dbo")
//@EnableJpaRepositories(basePackages = "com.alten.technicalchallenge.infrastructure.db.springdata.jpa.repository")
@EnableJpaAuditing
public class SpringDataJPAConf {

}
