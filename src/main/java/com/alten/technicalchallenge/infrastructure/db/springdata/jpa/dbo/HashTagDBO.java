package com.alten.technicalchallenge.infrastructure.db.springdata.jpa.dbo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

/**
 * Instantiates a new hash tag DBO.
 */
@Data
@Entity
@Table(name="HASHTAG")
public class HashTagDBO {

	/** The hashtag. */
	@Id
	private String hashtag;

	/** The count. */
	@Column(name="count")
	private long count;
}
