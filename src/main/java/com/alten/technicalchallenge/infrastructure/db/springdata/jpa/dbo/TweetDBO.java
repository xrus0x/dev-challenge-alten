package com.alten.technicalchallenge.infrastructure.db.springdata.jpa.dbo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

/**
 * Instantiates a new tweet DBO.
 */
@Data
@Entity
@Table(name="TWEET")
public class TweetDBO {

	/** The id. */
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	/** The user. */
	@Column(name="user")
	private String user;
	
	/** The text. */
	@Column(name="text",length = 5000)
	private String text;
	
	/** The localizacion. */
	@Column(name="localization")
	private String localizacion;
	
	/** The valid. */
	@Column(name="validated")
	private boolean valid;
}
