package com.alten.technicalchallenge.infrastructure.db.springdata.jpa.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.alten.technicalchallenge.infrastructure.db.springdata.jpa.dbo.HashTagDBO;

/**
 * The Interface HashTagJPARepository.
 */
@Repository
public interface HashTagJPARepository extends JpaRepository<HashTagDBO, String>{

}
