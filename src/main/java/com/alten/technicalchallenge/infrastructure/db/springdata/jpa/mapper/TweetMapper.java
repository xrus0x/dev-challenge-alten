package com.alten.technicalchallenge.infrastructure.db.springdata.jpa.mapper;

import java.util.List;

import org.mapstruct.Mapper;

import com.alten.technicalchallenge.domain.model.Tweet;
import com.alten.technicalchallenge.infrastructure.db.springdata.jpa.dbo.TweetDBO;

/**
 * The Interface TweetMapper.
 */
@Mapper(componentModel = "spring")
public interface TweetMapper {
	
	/**
	 * Tweet DBO to tweet.
	 *
	 * @param source the source
	 * @return the tweet
	 */
	Tweet tweetDBOToTweet(TweetDBO source);
	
	/**
	 * Tweet to tweet DBO.
	 *
	 * @param source the source
	 * @return the tweet DBO
	 */
	TweetDBO tweetToTweetDBO(Tweet source);
	
	/**
	 * List tweet DBO to list tweet.
	 *
	 * @param source the source
	 * @return the list
	 */
	List<Tweet> listTweetDBOToListTweet(List<TweetDBO> source);
	
	/**
	 * List tweet to list tweet DBO.
	 *
	 * @param source the source
	 * @return the list
	 */
	List<TweetDBO> listTweetToListTweetDBO(List<Tweet> source);
}
