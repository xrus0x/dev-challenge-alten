package com.alten.technicalchallenge.infrastructure.db.springdata.jpa.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import com.alten.technicalchallenge.domain.model.HashTag;
import com.alten.technicalchallenge.domain.repository.HashTagRepository;
import com.alten.technicalchallenge.infrastructure.db.springdata.jpa.dbo.HashTagDBO;
import com.alten.technicalchallenge.infrastructure.db.springdata.jpa.mapper.HashTagMapper;

import lombok.extern.slf4j.Slf4j;

/**
 * The Class HashTagDBORepository.
 */
@Repository
@Slf4j
public class HashTagDBORepository implements HashTagRepository {

	/** The hash tag JPA repository. */
	private final HashTagJPARepository hashTagJPARepository;
	
	/** The mapper. */
	@Autowired
	private HashTagMapper mapper;

	/**
	 * Instantiates a new hash tag DBO repository.
	 *
	 * @param hashTagJPARepository the hash tag JPA repository
	 */
	public HashTagDBORepository(HashTagJPARepository hashTagJPARepository) {
		this.hashTagJPARepository = hashTagJPARepository;
		this.mapper = mapper;
	}

	/**
	 * Save.
	 *
	 * @param hashTag the hash tag
	 */
	@Override
	public void save(HashTag hashTag) {

		try {
			hashTagJPARepository.save(mapper.hashTagToHashtagDBO(hashTag));
		} catch (DataAccessException ex) {
			log.error(ex.getMessage());
		}

	}

	/**
	 * Update.
	 *
	 * @param hasTag the has tag
	 */
	@Override
	public void update(HashTag hasTag) {
		save(hasTag);
	}

	/**
	 * Gets the hash tags.
	 *
	 * @return the hash tags
	 */
	@Override
	public List<HashTag> getHashTags() {
		try {
			return mapper.listHashTagDBOToListHashTag(hashTagJPARepository.findAll());
		} catch (DataAccessException ex) {
			log.error(ex.getMessage());
		}
		return null;
	}

	/**
	 * Gets the by id.
	 *
	 * @param hashTagId the hash tag id
	 * @return the by id
	 */
	@Override
	public HashTag getById(String hashTagId) {
		Optional<HashTagDBO> optional = hashTagJPARepository.findById(hashTagId);
		
		if(optional.isPresent()) {
			return mapper.hashTagDBOToHashTag(optional.get());
		}else {
			return null;
		}
		
	}

}
