package com.alten.technicalchallenge.infrastructure.db.springdata.jpa.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.alten.technicalchallenge.infrastructure.db.springdata.jpa.dbo.TweetDBO;

/**
 * The Interface TweetJPARepository.
 */
@Repository
public interface TweetJPARepository extends JpaRepository<TweetDBO, Long> {
	
	/**
	 * Find by user and valid.
	 *
	 * @param user the user
	 * @param valid the valid
	 * @return the list
	 */
	List<TweetDBO> findByUserAndValid(String user, boolean valid);
}
