package com.alten.technicalchallenge.infrastructure.twitter.filter.rules;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import twitter4j.Status;

/**
 * The Class FollowerFilterRule.
 */
@Component
public class FollowerFilterRule implements StatusFilterRule {

	/** The followers number. */
	@Value("${twitter.filters.followersNumber}")
	private int followersNumber;
	
	/**
	 * Run.
	 *
	 * @param data the data
	 * @return true, if successful
	 */
	@Override
	public boolean run(Status data) {
		
		return (data.getUser().getFollowersCount()>followersNumber);
	}

}
