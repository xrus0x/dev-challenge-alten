package com.alten.technicalchallenge.infrastructure.twitter.filter.rules;

import org.springframework.stereotype.Component;

import twitter4j.Status;

/**
 * The Interface StatusFilterRule.
 */
@Component
public interface StatusFilterRule {
	
	/**
	 * Run.
	 *
	 * @param data the data
	 * @return true, if successful
	 */
	boolean run(Status data);
}
