package com.alten.technicalchallenge.infrastructure.twitter.stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.integration.endpoint.MessageProducerSupport;
import org.springframework.messaging.MessageChannel;
import org.springframework.stereotype.Component;

import com.alten.technicalchallenge.domain.model.HashTag;
import com.alten.technicalchallenge.domain.model.Tweet;
import com.alten.technicalchallenge.domain.service.HashTagService;
import com.alten.technicalchallenge.domain.service.TweetService;
import com.alten.technicalchallenge.infrastructure.twitter.filter.StatusFilterRuleRunner;

import lombok.extern.slf4j.Slf4j;
import twitter4j.HashtagEntity;
import twitter4j.StallWarning;
import twitter4j.Status;
import twitter4j.StatusAdapter;
import twitter4j.TwitterStream;

/**
 * The Class TwitterMessageProducer.
 *
 * @author Rus0
 */

/** The Constant log. */
@Slf4j
@Component
public class TwitterMessageProducer extends MessageProducerSupport {

	/** The twitter stream. */
	private final TwitterStream twitterStream;
	
	/** The tweet service. */
	private final TweetService tweetService;
	
	/** The hash tag service. */
	private final HashTagService hashTagService;

	/** The status listener. */
	private StatusListener statusListener;
	
	/** The filter rule runner. */
	private StatusFilterRuleRunner filterRuleRunner;

	/** The allowed languages. */
	@Value("${twitter.filters.languages}")
	private String allowedLanguages;

	/**
	 * Instantiates a new twitter message producer.
	 *
	 * @param twitterStream the twitter stream
	 * @param outputChannel the output channel
	 * @param filterRuleRunner the filter rule runner
	 * @param tweetService the tweet service
	 * @param hashTagService the hash tag service
	 */
	@Autowired
	public TwitterMessageProducer(TwitterStream twitterStream, MessageChannel outputChannel,
			StatusFilterRuleRunner filterRuleRunner, TweetService tweetService, HashTagService hashTagService) {
		this.twitterStream = twitterStream;
		setOutputChannel(outputChannel);
		this.filterRuleRunner = filterRuleRunner;
		this.tweetService = tweetService;
		this.hashTagService = hashTagService;
	}

	/**
	 * On init.
	 */
	@Override
	protected void onInit() {
		super.onInit();
		statusListener = new StatusListener();
	}

	/**
	 * Do start.
	 */
	@Override
	public void doStart() {
		twitterStream.addListener(statusListener);
		twitterStream.sample();
	}

	/**
	 * Do stop.
	 */
	@Override
	public void doStop() {
		twitterStream.cleanUp();
		twitterStream.clearListeners();
	}

	/**
	 * Gets the status listener.
	 *
	 * @return the status listener
	 */
	StatusListener getStatusListener() {
		return statusListener;
	}

	/**
	 * The listener interface for receiving status events.
	 * The class that is interested in processing a status
	 * event implements this interface, and the object created
	 * with that class is registered with a component using the
	 * component's <code>addStatusListener<code> method. When
	 * the status event occurs, that object's appropriate
	 * method is invoked.
	 *
	 * @see StatusEvent
	 */
	class StatusListener extends StatusAdapter {

		/**
		 * On status.
		 *
		 * @param status the status
		 */
		@Override
		public void onStatus(Status status) {
			// Filter status conditions
			if (filterRuleRunner.runRules(status)) {
				log.debug("Status User-> " + status.getUser().getEmail());
				log.debug("Text ->" + status.getText());
				log.debug("Language ->" + status.getLang());
				log.debug("Followers ->" + status.getUser().getFollowersCount());
				log.debug("**************************");

				// Persist tweet
				Tweet tweet = createTweetFromStatus(status);
				tweetService.save(tweet);

				// Persist HashTag
				if (status.getHashtagEntities().length > 0) {

					for (HashtagEntity hashTagEntity : status.getHashtagEntities()) {
						HashTag hashTag = createHashTagFromStatus(hashTagEntity);
						hashTagService.save(hashTag);
					}

				}
			}
		}

		/**
		 * Creates the hash tag from status.
		 *
		 * @param hashTagEntity the hash tag entity
		 * @return the hash tag
		 */
		private HashTag createHashTagFromStatus(HashtagEntity hashTagEntity) {
			HashTag hashTag = new HashTag();
			hashTag.setHashtag(hashTagEntity.getText());
			hashTag.setCount(0);

			return hashTag;
		}

		/**
		 * Creates the tweet from status.
		 *
		 * @param status the status
		 * @return the tweet
		 */
		private Tweet createTweetFromStatus(Status status) {
			Tweet tweet = new Tweet();
			tweet.setUser(status.getUser().getName());//Use User name for "User" because the email always null.
			tweet.setText(status.getText());
			tweet.setLocalizacion(status.getLang());
			tweet.setValid(false);

			return tweet;
		}

		/**
		 * On exception.
		 *
		 * @param ex the ex
		 */
		@Override
		public void onException(Exception ex) {
			log.error(ex.getMessage(), ex);
		}

		/**
		 * On stall warning.
		 *
		 * @param warning the warning
		 */
		@Override
		public void onStallWarning(StallWarning warning) {
			log.warn(warning.toString());
		}

	}

}
