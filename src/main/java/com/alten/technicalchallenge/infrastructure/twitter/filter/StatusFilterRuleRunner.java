package com.alten.technicalchallenge.infrastructure.twitter.filter;

import java.util.List;

import org.springframework.stereotype.Component;

import com.alten.technicalchallenge.infrastructure.twitter.filter.rules.StatusFilterRule;

import lombok.AllArgsConstructor;
import twitter4j.Status;

/**
 * The Class StatusFilterRuleRunner.
 */


@Component
@AllArgsConstructor
public class StatusFilterRuleRunner {
	
	/** The rules. */
	private final List<StatusFilterRule> rules;
	
	/**
	 * Run rules.
	 *
	 * @param data the data
	 * @return true, if successful
	 */
	public boolean runRules(Status data) {
		
		boolean allow = true;
		
		for(int i = 0; i<rules.size() && allow; i++) {
			allow = rules.get(i).run(data);
		}
		return allow; 
	}
}
