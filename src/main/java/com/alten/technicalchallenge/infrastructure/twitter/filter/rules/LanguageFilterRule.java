package com.alten.technicalchallenge.infrastructure.twitter.filter.rules;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import twitter4j.Status;

/**
 * The Class LanguageFilterRule.
 */
@Component
public class LanguageFilterRule implements StatusFilterRule {

	/** The languages. */
	@Value("${twitter.filters.languages}")
	private String languages;
	
	/**
	 * Run.
	 *
	 * @param data the data
	 * @return true, if successful
	 */
	@Override
	public boolean run(Status data) {
		return languages.contains(data.getLang());
	}

}
