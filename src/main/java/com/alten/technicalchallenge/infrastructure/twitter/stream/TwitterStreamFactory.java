/*
 * 
 */
package com.alten.technicalchallenge.infrastructure.twitter.stream;

import org.springframework.beans.factory.config.AbstractFactoryBean;
import twitter4j.TwitterStream;

/**
 * A factory for creating TwitterStream objects.
 */
public class TwitterStreamFactory extends AbstractFactoryBean<TwitterStream> {

  /**
   * Gets the object type.
   *
   * @return the object type
   */
  @Override
  public Class<?> getObjectType() {
    return TwitterStream.class;
  }

  /**
   * Creates a new TwitterStream object.
   *
   * @return the twitter stream
   */
  @Override
  protected TwitterStream createInstance() {
    return new twitter4j.TwitterStreamFactory().getInstance();
  }

  /**
   * Destroy instance.
   *
   * @param twitterStream the twitter stream
   */
  @Override
  protected void destroyInstance(TwitterStream twitterStream) {
    twitterStream.shutdown();
  }

}
