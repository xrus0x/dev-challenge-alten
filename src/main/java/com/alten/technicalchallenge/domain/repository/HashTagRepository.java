package com.alten.technicalchallenge.domain.repository;

import java.util.List;

import com.alten.technicalchallenge.domain.model.HashTag;

/**
 * The Interface HashTagRepository.
 */
public interface HashTagRepository {

	/**
	 * Save.
	 *
	 * @param hashTag the hash tag
	 */
	void save(HashTag hashTag);
	
	/**
	 * Update.
	 *
	 * @param hasTag the has tag
	 */
	void update(HashTag hasTag);
	
	/**
	 * Gets the by id.
	 *
	 * @param hashTagId the hash tag id
	 * @return the by id
	 */
	HashTag getById(String hashTagId);
	
	/**
	 * Gets the hash tags.
	 *
	 * @return the hash tags
	 */
	List<HashTag> getHashTags();
	
}
