package com.alten.technicalchallenge.domain.repository;

import java.util.List;

import com.alten.technicalchallenge.domain.model.Tweet;

/**
 * The Interface TweetRepository.
 */
public interface TweetRepository {

	/**
	 * Gets the all.
	 *
	 * @return the all
	 */
	List<Tweet> getAll();
	
	/**
	 * Gets the validated twee by user.
	 *
	 * @param user the user
	 * @return the validated twee by user
	 */
	List<Tweet> getValidatedTweeByUser(String user);
	
	/**
	 * Validate tweet.
	 *
	 * @param idTweet the id tweet
	 */
	void validateTweet(long idTweet);
	
	/**
	 * Save.
	 *
	 * @param tweet the tweet
	 */
	void save(Tweet tweet);
}
