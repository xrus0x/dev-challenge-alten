package com.alten.technicalchallenge.domain.service;

import java.util.List;

import com.alten.technicalchallenge.domain.model.Tweet;

/**
 * The Interface TweetService.
 */
public interface TweetService {

	/**
	 * Save.
	 *
	 * @param tweet the tweet
	 */
	public void save(Tweet tweet);
	
	/**
	 * Validate tweet.
	 *
	 * @param tweetId the tweet id
	 */
	public void validateTweet(long tweetId);
	
	/**
	 * Gets the validated tweet by user.
	 *
	 * @param userId the user id
	 * @return the validated tweet by user
	 */
	public List<Tweet> getValidatedTweetByUser(String userId);
	
	/**
	 * Gets the tweets list.
	 *
	 * @return the tweets list
	 */
	public List<Tweet> getTweetsList();
}
