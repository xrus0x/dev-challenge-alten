package com.alten.technicalchallenge.domain.service;

import java.util.List;

import com.alten.technicalchallenge.domain.model.HashTag;

/**
 * The Interface HashTagService.
 */
public interface HashTagService {

	/**
	 * Save.
	 *
	 * @param hashTag the hash tag
	 */
	public void save(HashTag hashTag);
	
	/**
	 * Gets the hash tag list.
	 *
	 * @return the hash tag list
	 */
	public List<HashTag> getHashTagList();
}
