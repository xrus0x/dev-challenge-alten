package com.alten.technicalchallenge.domain.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.alten.technicalchallenge.domain.model.Tweet;
import com.alten.technicalchallenge.domain.repository.TweetRepository;

/**
 * The Class TweetServiceImpl.
 */
@Service
public class TweetServiceImpl implements TweetService {

	/** The tweet repository. */
	private final TweetRepository tweetRepository;
	
	
	/**
	 * Instantiates a new tweet service impl.
	 *
	 * @param tweetRepository the tweet repository
	 */
	public TweetServiceImpl(TweetRepository tweetRepository) {
		this.tweetRepository = tweetRepository;
	}

	/**
	 * Save.
	 *
	 * @param tweet the tweet
	 */
	@Override
	public void save(Tweet tweet) {
		tweetRepository.save(tweet);
	}

	/**
	 * Validate tweet.
	 *
	 * @param tweetId the tweet id
	 */
	@Override
	public void validateTweet(long tweetId) {
		tweetRepository.validateTweet(tweetId);
	}

	/**
	 * Gets the validated tweet by user.
	 *
	 * @param userId the user id
	 * @return the validated tweet by user
	 */
	@Override
	public List<Tweet> getValidatedTweetByUser(String userId) {
		return tweetRepository.getValidatedTweeByUser(userId);
	}

	/**
	 * Gets the tweets list.
	 *
	 * @return the tweets list
	 */
	@Override
	public List<Tweet> getTweetsList() {
		return tweetRepository.getAll();
	}

}
