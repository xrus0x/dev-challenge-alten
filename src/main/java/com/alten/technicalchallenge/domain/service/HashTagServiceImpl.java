package com.alten.technicalchallenge.domain.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.alten.technicalchallenge.domain.model.HashTag;
import com.alten.technicalchallenge.infrastructure.db.springdata.jpa.dbo.HashTagDBO;
import com.alten.technicalchallenge.infrastructure.db.springdata.jpa.mapper.HashTagMapper;
import com.alten.technicalchallenge.infrastructure.db.springdata.jpa.repository.HashTagJPARepository;

/**
 * The Class HashTagServiceImpl.
 */
@Repository
public class HashTagServiceImpl implements HashTagService {

	/** The hash tag JPA repository. */
	private final HashTagJPARepository hashTagJPARepository;

	/** The mapper. */
	@Autowired
	private HashTagMapper mapper;

	/**
	 * Instantiates a new hash tag service impl.
	 *
	 * @param hashTagRepository the hash tag repository
	 */
	public HashTagServiceImpl(HashTagJPARepository hashTagRepository) {
		this.hashTagJPARepository = hashTagRepository;
	}

	/**
	 * Save.
	 *
	 * @param hashTag the hash tag
	 */
	@Override
	public void save(HashTag hashTag) {
		Optional<HashTagDBO> optional = hashTagJPARepository.findById(hashTag.getHashtag());
		if (!optional.isPresent()) {
			hashTagJPARepository.save(mapper.hashTagToHashtagDBO(hashTag));
		} else {
			HashTagDBO updateHashTag = optional.get();
			updateHashTag.setCount(updateHashTag.getCount() + 1);
			hashTagJPARepository.save(updateHashTag);
		}
	}

	/**
	 * Gets the hash tag list.
	 *
	 * @return the hash tag list
	 */
	@Override
	public List<HashTag> getHashTagList() {
		return mapper.listHashTagDBOToListHashTag(hashTagJPARepository.findAll());
	}

}
