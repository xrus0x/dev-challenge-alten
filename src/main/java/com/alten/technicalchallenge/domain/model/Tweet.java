package com.alten.technicalchallenge.domain.model;

import lombok.Data;

/**
 * Instantiates a new tweet.
 */
@Data
public class Tweet {

	/** The id. */
	private long id;
	
	/** The user. */
	private String user;
	
	/** The text. */
	private String text;
	
	/** The localizacion. */
	private String localizacion;
	
	/** The valid. */
	private boolean valid;
}
