package com.alten.technicalchallenge.domain.model;

import lombok.Data;

/**
 * Instantiates a new hash tag.
 */
@Data
public class HashTag {

	/** The hashtag. */
	private String hashtag;
	
	/** The count. */
	private long count;
}
