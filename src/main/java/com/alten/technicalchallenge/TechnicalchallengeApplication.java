package com.alten.technicalchallenge;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan({"com.alten.technicalchallenge"})
public class TechnicalchallengeApplication {

	public static void main(String[] args) {
		SpringApplication.run(TechnicalchallengeApplication.class, args);
	}

}
