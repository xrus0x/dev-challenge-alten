# README #

Prueba técnica para el proceso de ALTEN

Una vez iniciado el proyecto con springboot se puede acceder a la consola de H2 y al swagger en las siguientes url:

Consola de H2 -> http://localhost:8080/h2-ui 
	driver class: org.h2.Driver
	JDBC URL: jdbc:h2:mem:testdb
	User: sa
	Password: (blank)
	
Swagger -> http://localhost:8080/swagger-ui.html